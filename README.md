# tesh
_Projet de RS (Réseaux / Systèmes) de 2e année_

Shell interactif écrit en C avec l'aide de flex et bison.

## Fonctionnalités

* commandes built-ins (_cd_, _fg_, _bg_)
* prompt
* enchaînements conditionnels (`&&`, `||`)
* redirections d’entrées/sorties (`|`, `>`, `<`, `>>`)
* mode non interactif/script (lancer avec `-e`)
* commandes en arrière plan (`&`)
* autocompletion avec readline